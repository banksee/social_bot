from urllib.parse import urljoin

import requests

import config


class APIClientBaseError(Exception):
    """
    Base class for APIClient errors.
    """
    pass


class APIClientActionError(APIClientBaseError):
    """
    Represents invalid endpoint action error.
    """
    pass


class APIClient(object):
    """
    Client for communicating with the API.
    """
    api_url = config.API_URL

    # TODO: It'd be better to move this to some kind of action:endpoint_url map
    authenticate_endpoint = '/api/auth/'
    register_endpoint = '/api/register/'

    create_post_endpoint = '/api/posts/'
    like_post_endpoint = '/api/posts/{post_id}/like/'
    unlike_post_endpoint = '/api/posts/{post_id}/unlike/'

    _jw_token = None

    def __init__(self, email=None, password=None, jw_token=None, api_url=None):
        error_msg = ("You must provide either `email` "
                     "and `password` or `jw_token`")

        if not email and not password:
            assert jw_token is not None, error_msg
        elif jw_token is None:
            assert email is not None and password is not None, error_msg

        self._email = email
        self._password = password
        self._jw_token = jw_token

        self.api_url = api_url or self.api_url

    def register(self):
        url = self._build_url('register')
        return self._post(url, params={
            "email": self._email,
            "password": self._password,
        })

    def authenticate(self):
        url = self._build_url('authenticate')
        response = self._post(url, params={
            "email": self._email,
            "password": self._password,
        })

        if response.ok:
            self._jw_token = response.json().get("token")
        else:
            response.raise_for_status()

        return response

    def create_post(self, title, content):
        self._ensure_authenticated()

        url = self._build_url('create_post')
        return self._post(url, params={
            "title": title,
            "content": content
        })

    def like_post(self, post_id):
        self._ensure_authenticated()

        url = self._build_url('like_post', post_id=post_id)
        return self._post(url)

    def unlike_post(self, post_id):
        self._ensure_authenticated()

        url = self._build_url('unlike_post', post_id=post_id)
        return self._post(url)

    def _ensure_authenticated(self):
        if self._jw_token is None:
            self.authenticate()

    def _build_url(self, action, **url_format_kw):
        api_root = self._get_api_url()
        attr_name = '{}_endpoint'.format(action)

        endpoint_url = getattr(self, attr_name, None)
        if url_format_kw:
            endpoint_url = endpoint_url.format(**url_format_kw)

        if endpoint_url is not None:
            return urljoin(api_root, endpoint_url)

        raise APIClientActionError("Unsupported action: `{}`. "
                                   "Does the client have `{}` method?"
                                   .format(action, attr_name))

    def _get_api_url(self):
        return self.api_url

    def _get_auth_headers(self):
        if self._jw_token is not None:
            return {"Authorization": "JWT {}".format(self._jw_token)}
        return {}

    def _get(self, url, params=None, headers=None):
        raise self._call(url, params=params, headers=headers, method='get')

    def _post(self, url, params=None, headers=None):
        return self._call(url, params=params, headers=headers, method='post')

    def _call(self, url, params=None, headers=None, method='get'):
        assert method in ('get', 'post')
        handler = getattr(requests, method)

        headers = headers or {}

        auth_headers = self._get_auth_headers()
        headers.update(auth_headers)

        response = handler(url, json=params, headers=headers)
        if response.ok:  # TODO: What if token has been expired?
            return response
        else:
            # TODO: Error message in this case doesn't contain API's errors
            response.raise_for_status()
