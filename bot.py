import random

import config
import entities


class Bot(object):
    user_manager_class = entities.UserManager

    def __init__(self, number_of_users, max_likes_per_user,
                 max_posts_per_user):
        self.number_of_users = number_of_users
        self.max_likes_per_user = max_likes_per_user
        self.max_posts_per_user = max_posts_per_user

    def signup_users(self, number_of_users=None):
        """
        Performs a signup of the users.
        """
        print("Starting user registration..")
        number_of_users = number_of_users or self.number_of_users

        for _ in range(number_of_users):
            self.user_manager.register_random_user()

        print("{} users have been registered.".format(number_of_users))
        return self

    def create_random_posts(self):
        """
        Creates random amount (up to max_posts_per_user) of posts for each user
        """
        print("Starting random posts creation..")
        for user in self.user_manager:
            num_of_posts = random.randint(1, self.max_posts_per_user)

            for _ in range(num_of_posts):
                user.create_random_post()

        print("Random posts have been created.")
        return self

    def like_posts(self):
        """
        Each user randomly likes a posts of others
        """
        print("Starting to like random posts..")
        print("Max likes per user: {}".format(self.max_likes_per_user))
        users = sorted(
            self.user_manager, key=lambda u: len(u.get_posts()), reverse=True
        )

        for user in users:
            max_likes_reached = False

            while not max_likes_reached:
                posts_to_like = self._get_posts_to_like(curr_user=user)

                if not posts_to_like:
                    break

                post = random.choice(posts_to_like)
                user.like_post(post)

                user_likes = len(user.get_liked_posts())
                max_likes_reached = user_likes == self.max_likes_per_user

        print("Random posts have been liked.")
        return self

    def _get_posts_to_like(self, curr_user):
        """
        Returns all posts that is available for the `current_user` to like:
            - exclude current_user's posts
            - exclude all posts that are already liked by current user
            - include all posts from users who have
              at least one post with no likes
        """
        users = filter(lambda user: curr_user != user, self.user_manager)
        posts_to_like = []

        for user in users:
            user_posts = user.get_posts()

            no_likes_posts = any(len(post.liked_by) == 0 for post in user_posts)
            if no_likes_posts:
                # Exclude all posts that are already liked by current user
                posts = filter(lambda post: curr_user not in post.liked_by,
                               user_posts)
                posts_to_like.extend(posts)

        return posts_to_like

    @property
    def user_manager(self):
        """
        Initializes a user manager for this bot.
        """
        if not hasattr(self, "_user_manager"):
            self._user_manager = self.user_manager_class()
        return self._user_manager

    @classmethod
    def from_config(cls):
        """
        Initializes bot from the configuration script.
        Configuration file should contain constants:
            - BOT_NUMBER_OF_USERS (int)
            - BOT_MAX_POSTS_PER_USER (int)
            - BOT_MAX_LIKES_PER_USER (int)
        :return: Bot instance
        """
        return cls(
            number_of_users=config.BOT_NUMBER_OF_USERS,
            max_likes_per_user=config.BOT_MAX_LIKES_PER_USER,
            max_posts_per_user=config.BOT_MAX_POSTS_PER_USER,
        )

    def __repr__(self):
        return ("Bot(number_of_users={}, max_likes_per_user={}, "
                "max_posts_per_user={})"
                .format(self.number_of_users,
                        self.max_likes_per_user,
                        self.max_posts_per_user))


if __name__ == '__main__':
    bot = Bot.from_config()  # TODO: Allow to specify different configs?
    bot.signup_users().create_random_posts().like_posts()
