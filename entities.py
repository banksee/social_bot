import config
from client import APIClient
from utils import get_random_str


class Post(object):
    def __init__(self, post_id, user, title, content, liked_by=None):
        self.post_id = post_id
        self.user = user
        self.title = title
        self.content = content
        self.liked_by = liked_by or set()

    def add_like(self, user):
        self.liked_by.add(user)

    def remove_like(self, user):
        try:
            self.liked_by.remove(user)
        except KeyError:
            pass

    def __eq__(self, other):
        if not isinstance(other, Post):
            return NotImplemented
        return self.post_id == other.post_id

    def __hash__(self):
        return hash((self.post_id, self.user.user_id))

    def __repr__(self):
        return ("Post(post_id={}, user={}, title={}, content={}, likes={}"
                .format(self.post_id, self.user, self.title, self.content,
                        self.liked_by))


class User(object):
    api_client_class = APIClient

    def __init__(self, user_id, email, password, client=None):
        self.user_id = user_id
        self.email = email
        self.password = password

        if client is None:
            client = self.get_api_client(email=email, password=password)

        self.client = client

        self._posts = set()
        self._liked_posts = set()

    def get_posts(self):
        return self._posts

    def get_liked_posts(self):
        return self._liked_posts

    def create_post(self, title, content):
        response = self.client.create_post(title, content)
        data = response.json()

        post_id = data.get("id")
        post = Post(post_id, self, title, content)
        self._posts.add(post)

        return post

    def create_random_post(self):
        title = get_random_str()
        content = get_random_str()
        return self.create_post(title, content)

    def like_post(self, post):
        self.client.like_post(post.post_id)

        self._liked_posts.add(post)
        post.add_like(self)

        return self

    def unlike_post(self, post):
        if post in self._liked_posts:
            self.client.unlike_post(post.post_id)
            self._liked_posts.remove(post)
            post.remove_like(self)

        return self

    def get_api_client(self, *args, **kwargs):
        return self.api_client_class(*args, **kwargs)

    @classmethod
    def signup(cls, email, password):
        """
        Registers a new User with provided credentials.
        :param email: Email to register a User with.
        :param password: User's password to register with.
        :return: User instance.
        """
        client = cls.api_client_class(email=email, password=password)
        response = client.register()

        response_data = response.json()
        user_id = response_data.get("id")

        return cls(
            user_id=user_id, email=email, password=password, client=client
        )

    def __eq__(self, other):
        if not isinstance(other, User):
            return NotImplemented
        return self.user_id == other.user_id

    def __hash__(self):
        return hash(self.user_id)

    def __repr__(self):
        return ("User(user_id={}, email={}, password={})"
                .format(self.user_id, self.email, self.password))


class UserManager(object):
    """
    Intermediate class for holding a set of User instances.
    """
    def __init__(self, users=None):
        self._users = users or set()
        self._used_emails = set()

    def add_user(self, user):
        """
        Adds a user to the manager.
        :param user: User instance.
        """
        self._users.add(user)
        return self

    def remove_user(self, user):
        """
        Removes a user from the manager.
        :param user: User instance.
        """
        self._users.remove(user)
        return self

    def register_user(self, email, password):
        """
        Registers a new user with the given email and password.
        And adds it to the manager user list.
        :param email: Email of the future user.
        :param password: Password of the future user.
        :return: User instance.
        """
        user = User.signup(email, password)
        self.add_user(user)
        return user

    def register_random_user(self, password_length=5):
        """
        Generates a random email and password and runs a registration.
        :param password_length: Length of the password.
        :return: User instance.
        """
        email = self.get_random_email()
        password = self.get_random_password(password_length)

        return self.register_user(email=email, password=password)

    def get_random_email(self):
        """
        Returns random valid email that is not have been used in registration.
        :return: Generated email string.
        """
        for email in config.VALID_EMAILS:
            if email not in self._used_emails:
                self._used_emails.add(email)
                return email

    @staticmethod
    def get_random_password(length=5):
        return get_random_str(length)

    def __iter__(self):
        return iter(self._users)

    def __len__(self):
        return len(self._users)

    def __contains__(self, item):
        return item in self._users

    def __repr__(self):
        return "UserManager(users={})".format(self._users)
