import string
import random


def get_random_str(length=5):
    """
    Generates a random string based on ASCII characters and numbers
    :param length: Length of the resulting string
    """
    numbers = "01234567890"
    chars = "".join((string.ascii_uppercase, string.ascii_lowercase, numbers,))

    result = []
    for _ in range(length):
        result.append(random.choice(chars))

    return "".join(result)
